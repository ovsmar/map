var map = L.map("map").setView([48.856245, 2.350559], 8);

L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
  attribution:
    '&copy; <a href="https://www.openstreetmap.org/search?query=France#map=12/43.6126/3.9207">OpenStreetMap</a> contributors',
}).addTo(map);

L.marker([48.856245, 2.350559]).addTo(map).bindPopup("LOL.").openPopup();

var popup = L.popup()
  .setLatLng([48.856245, 2.350559])
  .setContent("I am a standalone popup.")
  .openOn(map);

function onMapClick(e) {
  alert("You clicked the map at " + e.latlng);
}

map.on("click", onMapClick);

var popup = L.popup();

function onMapClick(e) {
  popup
    .setLatLng(e.latlng)
    .setContent("You clicked the map at " + e.latlng.toString())
    .openOn(map);
}

map.on("click", onMapClick);
